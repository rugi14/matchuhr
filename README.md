# Matchuhr
a gameclock for sport. Designed for floorball (swiss unihockey rules)

## Warning
This is not fully tested. Wrote from a apprentice. Usage at your own risk.

## Display
Gametime			4x7-Seg <br>
Goals (Points)		LCD <br>
Penalty-Time		LCD (one for both Teams) <br>
Period				LCD <br>
Timeinformation		RGB <br>

## Usage
### RGB
green		time is running <br>
red			time is stopped <br>
blue		pause <br>
white		Timeout (30s) <br>
orange		Shootout <br>
Pink		End of game <br>

### Buttons
ts0			start/stop <br>
ts1			goals guest <br>
ts2			goals home <br>
ts3			2'-guest <br>
ts4			2'-home <br>
ts5			5'-guest <br>
ts6			5'-home <br>
ts7			start Timeout <br>

## Envirement
Hardware: ELOB (https://www.s-tec.ch/produkte/zlv-mint-schulungsboard) with ATMega2560
Recomended Softwareenvirement: AtmelStudio 7.0
